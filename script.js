(function () {
    'use strict';

    this.$doRequestButton = document.querySelector('#doRequest');

    const DEFAULT_URL = 'https://api.awsli.com.br/v1/produto';
    const DEFAULT_API_KEY = 'eb4aa49b0e1e00d628f7';
    const DEFAULT_APPLICATION_KEY = '5dcaa08e-89c3-4445-8e2c';

    const HTTP_STATUS = {
        OK: 200,
        ACCEPTED: 201,
        NOT_FOUND: 404,
        INTERNAL_SERVER_ERROR: 500
    }

    function createRequestUrl(urlBase, apiKey, applicationKey){
        return `${urlBase ?? DEFAULT_URL}?chave_api=${apiKey ?? DEFAULT_API_KEY}&chave_aplicacao=${applicationKey ?? DEFAULT_APPLICATION_KEY}`;
    }

    function doSimpleRequest(method, data, urlBase, apiKey, applicationKey) {
        const requestInit = {
            method: method,
            body: JSON.stringify(data ?? ''),
        };
        
        const requestUrl = createRequestUrl(urlBase, apiKey, applicationKey);

        fetch(requestURL, requestInit)
        .then(response => {
            if (response.status === HTTP_STATUS.OK) {
                return response.json();
            }
        })
        .then(result => {
            console.log(result);
        })
        .catch(error => console.log('error', error));
    }

    const data = {
        id_externo: null,
        sku: "prod-pai123123",
        mpn: null,
        ncm: null,
        nome: "Produto 666 Master Plus",
        descricao_completa: "<strong>Desctição HTML do produto</strong>",
        ativo: false,
        destaque: false,
        peso: 0.45,
        altura: 2,
        largura: 12,
        profundidade: 6,
        tipo: "atributo",
        usado: false,
        removido: false
    };

    function initializeSimpleRequestListeners() {
        this.$doRequestButton.on('click', () => {
            doSimpleRequest('POST', data, null);
        });
    }

    initializeSimpleRequestListeners();
}());
